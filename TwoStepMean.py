from mrjob.job import MRJob
from mrjob.step import MRStep
import math
 

class MeanR2(MRJob):
    
    def steps(self):
        return [
            MRStep(mapper=self.mapper_partition, reducer=self.reducer_sum)
            # MRStep(reducer=self.reducer_average)
        ]

    def mapper_partition(self, _, line):
        # partition the values from column R2 into groups
        (id, time, R1, R2, R3, R4, R5, R6, R7, R8, Temp, Humidity) = line.split()

        try:
            num = float(R2)
            yield ("n", 1)
            yield ("R2", num)
        except ValueError:
            pass

    def reducer_sum(self, key, counts):
        # sum all key/value pairs    
        yield (key, sum(counts))


    # def reducer_average(self, values, counts):
        # i, totalV = 0, 0
        # for i in values:
            # totalV += i
            # yield "average of R2", sum(values)/sum(counts)

if __name__ == '__main__':
    MeanR2.run()