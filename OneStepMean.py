from mrjob.job import MRJob

 
class MeanR2(MRJob):
    
    def mapper(self, _, line):
        # yield the values from column R2
        if line.split()[3] != 'R2':
            yield "values", float(line.split()[3])
        

    def reducer(self, key, values):
        # sum all key/value pairs and divide by the count
        i, totalL, totalW = 0, 0, 0
        for i in values:
            totalL += 1
            totalW += i     
        yield "average of R2", totalW/float(totalL)

if __name__ == '__main__':
    MeanR2.run()